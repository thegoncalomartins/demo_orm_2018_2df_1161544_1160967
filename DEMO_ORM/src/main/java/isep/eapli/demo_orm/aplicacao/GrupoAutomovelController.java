/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.aplicacao;

import isep.eapli.demo_orm.bll.GrupoAutomovel;
import isep.eapli.demo_orm.persistencia.GrupoAutomovelRepositorio;
import isep.eapli.demo_orm.persistencia.GrupoAutomovelRepositorioJPAImpl;
import java.util.List;

/**
 *
 * @author eapli
 */
public class GrupoAutomovelController {

    public GrupoAutomovel registarGrupoAutomóvel(String nome, int portas,
            String classe) {
        GrupoAutomovel grupo1 = new GrupoAutomovel(nome, portas, classe);
        GrupoAutomovelRepositorioJPAImpl repo = new GrupoAutomovelRepositorioJPAImpl();
        return repo.add(grupo1);
    }

    public List<GrupoAutomovel> listarGruposAutomoveis() {
        return new GrupoAutomovelRepositorioJPAImpl().findAll();
    }

    public GrupoAutomovel procurarGrupoAutomovel(int id) {
        GrupoAutomovelRepositorioJPAImpl repo = new GrupoAutomovelRepositorioJPAImpl();
        return repo.findById(id);
    }
}
