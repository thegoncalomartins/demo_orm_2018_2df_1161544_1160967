package isep.eapli.demo_orm.aplicacao;

import java.util.List;

import isep.eapli.demo_orm.bll.Automovel;
import isep.eapli.demo_orm.bll.GrupoAutomovel;
import isep.eapli.demo_orm.persistencia.AutomovelRepositorioJPAImpl;

public class AutomovelController {

	public Automovel registarAutomovel(String matricula, GrupoAutomovel ga, int kms) {
		Automovel automovel = new Automovel(matricula, ga, kms);
		AutomovelRepositorioJPAImpl repo = new AutomovelRepositorioJPAImpl();
		return repo.add(automovel);
	}

	public List<Automovel> listarAutomoveis() {
		AutomovelRepositorioJPAImpl repo = new AutomovelRepositorioJPAImpl();

		return repo.findAll();
	}

	public List<Automovel> listarAutomoveisDeGrupoAutomovel(String nome) {
		AutomovelRepositorioJPAImpl repo = new AutomovelRepositorioJPAImpl();
		return repo.findAutomovelByGrupoAutomovel(nome);
	}

	public Automovel procurarAutoPorMatricula(String matricula) {
		AutomovelRepositorioJPAImpl repo = new AutomovelRepositorioJPAImpl();
		return repo.procurarAutoPorMatricula(matricula);
	}
}
