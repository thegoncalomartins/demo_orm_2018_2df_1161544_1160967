/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.apresentacao;

import java.util.List;

import isep.eapli.demo_orm.aplicacao.GrupoAutomovelController;
import isep.eapli.demo_orm.bll.GrupoAutomovel;
import isep.eapli.demo_orm.util.Console;

/**
 *
 * @author mcn
 */
public class GrupoAutomovelUI {

    private final GrupoAutomovelController controller = new GrupoAutomovelController();

    public void registarGA() {
        System.out.println("*** Registo Grupo Automóvel ***\n");
        String nome = Console.readLine("Nome:");
        int portas = Console.readInteger("Número de portas");
        String classe = Console.readLine("Classe:");
        GrupoAutomovel grupoAutomovel = controller.
                registarGrupoAutomóvel(nome, portas, classe);
        System.out.println("Grupo Automóvel" + grupoAutomovel);
    }

    public void listarGAs() {
        System.out.println("*** Listar Grupos Automóveis ***\n");
        List<GrupoAutomovel> grupos = controller.listarGruposAutomoveis();
        for (GrupoAutomovel grupo : grupos) {
            System.out.println(grupo);
        }
    }

    public void procurarGAPorID(int id) {
        System.out.println(controller.procurarGrupoAutomovel(id));
    }
}
