/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.apresentacao;

import isep.eapli.demo_orm.util.Console;

/**
 * @author mcn
 */
public class MainMenu {

	public static void mainLoop() {
		int opcao = 0;
		do {
			opcao = menu();

			switch (opcao) {
			case 0:
				System.out.println("FIM");
				break;
			case 1:
				new GrupoAutomovelUI().registarGA();
				break;

			case 2:
				new GrupoAutomovelUI().listarGAs();
				break;

			case 3:
				int id = Console.readInteger("Insira o ID do Grupo Automóvel");
				new GrupoAutomovelUI().procurarGAPorID(id);
				break;

			case 4:
				new RegistarAutomovelUI().registarAutomovel();
				break;
			case 5:
				new ListarAutomovelUI().listarAutomoveis();
				break;
			case 6:
				new ListarAutomovelUI().listarAutomoveisDeGrupoAutomovel();
				break;
			case 7:
				new ListarAutomovelUI().procurarAutoPorMatricula();
				break;

			default:
				System.out.println("Opção não reconhecida.");
				break;
			}
		} while (opcao != 0);

	}

	private static int menu() {
		int option = -1;
		System.out.println();
		System.out.println("=============================");
		System.out.println(" Rent a Car ");
		System.out.println("=============================\n");
		System.out.println("1.Registar Grupo Automóvel");
		System.out.println("2.Listar todos os Grupos Automóveis");
		System.out.println("3.Procurar Grupo Automóvel por ID");
		System.out.println("4.Registar Automóvel");
		System.out.println("5.Listar todos os Automóveis");
		System.out.println("6.Listar automóveis de grupo automóvel");
		System.out.println("7.Procurar automóvel por matrícula");

		System.out.println("=============================");
		System.out.println("0. Sair\n\n");
		option = Console.readInteger("Por favor escolha opção");
		return option;
	}
}
