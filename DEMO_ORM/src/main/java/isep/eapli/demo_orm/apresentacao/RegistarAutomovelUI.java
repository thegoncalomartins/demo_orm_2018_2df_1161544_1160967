package isep.eapli.demo_orm.apresentacao;

import isep.eapli.demo_orm.aplicacao.AutomovelController;
import isep.eapli.demo_orm.aplicacao.GrupoAutomovelController;
import isep.eapli.demo_orm.bll.Automovel;
import isep.eapli.demo_orm.bll.GrupoAutomovel;
import isep.eapli.demo_orm.util.Console;

public class RegistarAutomovelUI {

	private final GrupoAutomovelController grupoAutomovelController = new GrupoAutomovelController();
	private final AutomovelController automovelController = new AutomovelController();

	public void registarAutomovel() {
		System.out.println("*** Registo Automóvel ***\n");
		int id = Console.readInteger("ID do Grupo Automovel:");
		GrupoAutomovel ga = grupoAutomovelController.procurarGrupoAutomovel(id);
		while (ga == null) {
			System.out.println("ID do Grupo Automovel inválido.\n");
			id = Console.readInteger("ID do Grupo Automovel:");
			ga = grupoAutomovelController.procurarGrupoAutomovel(id);
		}
		String matricula = Console.readLine("Matrícula:");
		int kms = Console.readInteger("Kms:");
		Automovel automovel = automovelController.registarAutomovel(matricula, ga, kms);
		System.out.println("Automóvel" + automovel);
	}
}
