/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.apresentacao;

import java.util.List;

import isep.eapli.demo_orm.aplicacao.AutomovelController;
import isep.eapli.demo_orm.bll.Automovel;
import isep.eapli.demo_orm.util.Console;

/**
 * @author João Dias 1160967@isep.ipp.pt
 * @author Gonçalo Martins 1161544@isep.ipp.pt
 */
public class ListarAutomovelUI {

	/**
	 * O objeto controller.
	 */
	private final AutomovelController controller = new AutomovelController();

	/**
	 * Lista todos os automóveis.
	 */
	public void listarAutomoveis() {
		List<Automovel> la = controller.listarAutomoveis();
		if (la.isEmpty()) {
			System.out.println("Não existem automóveis registados no sistema.");
		} else {
			for (Automovel automovel : la) {
				System.out.println(automovel);
			}
		}

	}

	/**
	 * Procura automóvel por matrícula.
	 */
	public void procurarAutoPorMatricula() {
		String matricula = Console.readLine("Insira a matrícula:");
		Automovel auto = controller.procurarAutoPorMatricula(matricula);

		if (auto != null) {
			System.out.println(auto);
		} else {
			System.out.println("Não foi encontrado nenhum automóvel");
		}
	}

	public void listarAutomoveisDeGrupoAutomovel() {
		String nome = Console.readLine("Nome do grupo automóvel:");
		List<Automovel> list = controller.listarAutomoveisDeGrupoAutomovel(nome);
		if (list.isEmpty()) {
			System.out.println("Não existem automóveis desse grupo automóvel ou o grupo automóvel não existe.");
		} else {
			System.out.println(list);
		}
	}

}
