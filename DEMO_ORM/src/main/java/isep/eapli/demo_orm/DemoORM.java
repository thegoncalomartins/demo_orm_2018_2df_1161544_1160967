package isep.eapli.demo_orm;

import static isep.eapli.demo_orm.apresentacao.MainMenu.mainLoop;

/**
 * @author Gonçalo Martins 1161544
 * @author João Dias 1160967
 */
public class DemoORM {

    public static void main(String[] args) {
        mainLoop();
    }
}
