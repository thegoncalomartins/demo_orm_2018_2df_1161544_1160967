package isep.eapli.demo_orm.bll;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @author Gonçalo Martins 1161544
 * @author João Dias 1160967
 */
@Entity
public class Automovel {

    @Id
    @GeneratedValue
    private int idAuto;

    private String matricula;

    @ManyToOne
    private GrupoAutomovel grupo;
    private int kms;

    protected Automovel() {

    }

    public Automovel(String matricula, GrupoAutomovel grupo, int kms) {
        this.matricula = matricula;
        this.grupo = grupo;
        this.kms = kms;
    }

    /**
     * Obtém a matrícula do automóvel.
     *
     * @return a matrícula.
     */
    public String matricula() {
        return matricula;
    }

    public void alterarKms(int kms) {
        this.kms = kms;

    }

    @Override
    public String toString() {
        return String.format("Matricula: %s%nKms: %d%n%s", matricula, kms, grupo);

    }

}
