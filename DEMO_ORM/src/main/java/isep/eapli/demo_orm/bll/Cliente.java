package isep.eapli.demo_orm.bll;

import isep.eapli.demo_orm.util.DateTime;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Cliente {

    @Id
    private int NIF;

    private String nome;

    @Temporal(TemporalType.DATE)
    private DateTime dataNascimento;

    protected Cliente() {

    }

    public Cliente(int NIF, String nome, DateTime dataNascimento) {
        this.NIF = NIF;
        this.nome = nome;
        this.dataNascimento = dataNascimento;
    }
}
