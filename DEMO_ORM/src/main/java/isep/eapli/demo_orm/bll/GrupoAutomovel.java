package isep.eapli.demo_orm.bll;

import javax.persistence.*;

/**
 * @author João Dias 1160967@isep.ipp.pt
 * @author Gonçalo Martins 1161544@isep.ipp.pt
 */
@Entity
@Table(name = "GrupoAutomovel") // nome da tabela
public class GrupoAutomovel {
	/**
	 * ID do grupo automóvel.
	 */
	@Id
	@GeneratedValue // id gerado automaticamente pela base de dados
	@Column(name = "IDGRUPOAUTOMOVEL") // nome da coluna da base de dados
	private int idGrupoAutomovel;

	/**
	 * Nome do grupo.
	 */
	private String nome;

	/**
	 * Número de portas.
	 */
	private Integer portas;

	/**
	 * Classe do grupo.
	 */
	private String classe;

	/**
	 * Construtor de grupo automóvel.
	 */
	protected GrupoAutomovel() {

	}

	/**
	 * Construtor de grupo automóvel que recebe os atributos.
	 * 
	 * @param nome
	 *            o nome do grupo
	 * @param portas
	 *            o número de portas
	 * @param classe
	 *            a classe do grupo
	 */
	public GrupoAutomovel(String nome, int portas, String classe) {
		this.nome = nome;
		this.portas = portas;
		this.classe = classe;
	}

	/**
	 * Altera a classe deste grupo automóvel.
	 * 
	 * @param classe
	 *            nova classe.
	 */
	public void alterarClasse(String classe) {
		this.classe = classe;
	}

	/**
	 * Altera o número de portas deste grupo automóvel.
	 * 
	 * @param portas
	 *            novo número de portas.
	 */
	public void alterarPortas(Integer portas) {
		this.portas = portas;
	}

	@Override
	public String toString() {
		return String.format(
				"GrupoAutomovel\n" + "idGrupoAutomovel: %d\n" + "nome: %s\n" + "portas: %d\n" + "classe: %s\n",
				idGrupoAutomovel, nome, portas, classe);
	}
}
