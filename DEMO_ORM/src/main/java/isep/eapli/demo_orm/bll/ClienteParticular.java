package isep.eapli.demo_orm.bll;

import isep.eapli.demo_orm.util.DateTime;

import javax.persistence.Entity;

@Entity
public class ClienteParticular extends Cliente {

    private long NIB;

    protected ClienteParticular() {
        super();
    }

    public ClienteParticular(long nib, int NIF, String nome, DateTime dataNascimento) {
        super(NIF, nome, dataNascimento);
        this.NIB = nib;
    }
}
