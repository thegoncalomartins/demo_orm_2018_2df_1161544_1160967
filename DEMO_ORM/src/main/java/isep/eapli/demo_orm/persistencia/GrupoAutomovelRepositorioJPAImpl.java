/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.persistencia;


import isep.eapli.demo_orm.bll.GrupoAutomovel;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

/**
 * @author eapli
 */
public class GrupoAutomovelRepositorioJPAImpl extends JpaRepository<GrupoAutomovel, Integer> implements GrupoAutomovelRepositorio {


    private static final String PERSISTENCE_UNIT_NAME = "DEMO_ORMPU";

    /**
     * inserts an entity GrupoAutomovel
     *
     * @param grupoAutomovel
     * @return the persisted entity
     */
    @Override
    public GrupoAutomovel add(GrupoAutomovel grupoAutomovel) {
        return super.add(grupoAutomovel);
    }


    /**
     * reads an entity GrupoAutomovel given its ID
     *
     * @param id
     * @return
     */
    @Override
    public GrupoAutomovel findById(Integer id) {
        return super.findById(id);
    }

    /**
     * Returns the List of all entities in the persistence store
     *
     * @return
     */
    //@SuppressWarnings("unchecked")
    @Override
    public List<GrupoAutomovel> findAll() {
        return super.findAll();
    }

    @Override
    protected String persistenceUnitName() {
        return PERSISTENCE_UNIT_NAME;
    }

}
