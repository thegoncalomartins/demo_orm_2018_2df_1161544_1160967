package isep.eapli.demo_orm.persistencia;

import isep.eapli.demo_orm.bll.Automovel;

import javax.persistence.Query;
import java.util.List;

public class AutomovelRepositorioJPAImpl extends JpaRepository<Automovel, Integer> implements AutomovelRepositorio {

    private static final String PERSISTENCE_UNIT_NAME = "DEMO_ORMPU";

    /**
     * inserts an entity Automovel
     *
     * @param automovel
     * @return the persisted entity
     */
    @Override
    public Automovel add(Automovel automovel) {
        return super.add(automovel);
    }

    /**
     * reads an entity Automovel given its ID
     *
     * @param id
     * @return
     */
    @Override
    public Automovel findById(Integer id) {
        return super.findById(id);
    }

    /**
     * Returns the List of all entities in the persistence store
     *
     * @return
     */
    // @SuppressWarnings("unchecked")
    @Override
    public List<Automovel> findAll() {
        return super.findAll();
    }

    @Override
    protected String persistenceUnitName() {
        return PERSISTENCE_UNIT_NAME;
    }

    /**
     * Procura automóvel por matrícula.
     *
     * @param matricula a matrícula a procurar.
     * @return o automóvel cuja matrícula é a passada por parâmetro ou null, caso
     * não encontre.
     */
    @Override
    public Automovel procurarAutoPorMatricula(String matricula) {
        try {
            Query query = super.entityManager().createQuery("SELECT auto FROM Automovel auto WHERE auto.matricula = :m");
            query.setParameter("m", matricula);

            return (Automovel) query.getSingleResult();
        } catch (javax.persistence.NoResultException e) {
            return null;
        }
    }

    @Override
    public List<Automovel> findAutomovelByGrupoAutomovel(String nome) {
        Query query = super.entityManager().createQuery("SELECT a FROM Automovel a JOIN a.grupo ga WHERE ga.nome = :n");
        query.setParameter("n", nome);
        return query.getResultList();
    }
}
