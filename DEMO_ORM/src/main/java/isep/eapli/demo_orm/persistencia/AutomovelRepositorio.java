package isep.eapli.demo_orm.persistencia;

import isep.eapli.demo_orm.bll.Automovel;

import java.util.List;

public interface AutomovelRepositorio {

    /**
     * Devolve a lista de automóveis cujo nome do grupo automóvel é igual ao passado
     * por parâmetro.
     *
     * @param nome o nome do grupo automóvel
     * @return
     */
    public List<Automovel> findAutomovelByGrupoAutomovel(String nome);

    /**
     * Procura automóvel por matrícula.
     *
     * @param matricula a matrícula a procurar.
     * @return o automóvel cuja matrícula é a passada por parâmetro ou null, caso
     * não encontre.
     */
    public Automovel procurarAutoPorMatricula(String matricula);
}
